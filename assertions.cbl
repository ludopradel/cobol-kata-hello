       IDENTIFICATION DIVISION.
       PROGRAM-ID. ASSERT-EQUAL IS INITIAL.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       LINKAGE SECTION.
           COPY hello-vars.

       PROCEDURE DIVISION USING MESSAGE-OUTPUT, EXPECTED.
       MAIN.
           IF MESSAGE-OUTPUT NOT EQUAL EXPECTED THEN
              DISPLAY 'FAIL: ',
               '"', MESSAGE-OUTPUT, '" ≠ "', EXPECTED, '"',
           ELSE
              DISPLAY 'PASS.',
           END-IF 
           .

