       IDENTIFICATION DIVISION.
       PROGRAM-ID. hello IS INITIAL.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
        77 INPUT-MOD3 PIC 9.
        77 INPUT-MOD5 PIC 9.
        01 HELLO PIC X(5) VALUE 'HELLO'.


       LINKAGE SECTION.
           COPY hello-vars.

       PROCEDURE DIVISION USING INPUT-NAME,
                                MESSAGE-OUTPUT.
       MAIN.
           IF INPUT-NAME EQUAL SPACE THEN
               MOVE HELLO TO MESSAGE-OUTPUT
           ELSE 
             STRING HELLO DELIMITED BY SPACE 
              ' '   DELIMITED BY SIZE
              INPUT-NAME DELIMITED BY SPACE
              INTO MESSAGE-OUTPUT
           END-IF
           .
