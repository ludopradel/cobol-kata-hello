       IDENTIFICATION DIVISION.
       PROGRAM-ID. MAIN-TEST.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
        01 ACTUAL   PIC X(4).
           COPY hello-vars.

       PROCEDURE DIVISION.
       MAIN-TEST.
           PERFORM should-return-hello-when-empty
           PERFORM should-return-hello-name 
           PERFORM should-trim-whitespace
           STOP RUN.
       
           
       should-return-hello-when-empty.
           DISPLAY 'should-return-hello-when-empty'
      *    vérifie que HELLO est retourné lorsque INPUT-NAME est vide 
      *    GIVEN

      *    WHEN

      *    THEN
           
       .
       
       should-return-hello-name.
           DISPLAY 'should-return-hello-name'
      *    vérifie que HELLO Ludo est retourné lorsque INPUT-NAME est égal à Ludo 
      *    GIVEN

      *    WHEN

      *    THEN

       .
       
       should-trim-whitespace.
           DISPLAY 'should-trim-whitespace'
      *    vérifie que HELLO Ludo est retourné lorsque INPUT-NAME est égal à "  Ludo  " 
      *    GIVEN

      *    WHEN

      *    THEN
      
       .


